package model;

public class Client {


 
    private System.ComponentModel.IContainer components = null;

   
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.userTextBox = new System.Windows.Forms.TextBox();
        this.passTextBox = new System.Windows.Forms.TextBox();
        this.label1 = new System.Windows.Forms.Label();
        this.label2 = new System.Windows.Forms.Label();
        this.button1 = new System.Windows.Forms.Button();
        this.SuspendLayout();
        // 
        // userTextBox
        // 
        this.userTextBox.Location = new System.Drawing.Point(34, 66);
        this.userTextBox.Name = "userTextBox";
        this.userTextBox.Size = new System.Drawing.Size(108, 20);
        this.userTextBox.TabIndex = 0;
        // 
        // passTextBox
        // 
        this.passTextBox.Location = new System.Drawing.Point(34, 105);
        this.passTextBox.Name = "passTextBox";
        this.passTextBox.Size = new System.Drawing.Size(110, 20);
        this.passTextBox.TabIndex = 1;
        this.passTextBox.UseSystemPasswordChar = true;
        // 
        // label1
        // 
        this.label1.AutoSize = true;
        this.label1.Location = new System.Drawing.Point(33, 50);
        this.label1.Name = "label1";
        this.label1.Size = new System.Drawing.Size(60, 13);
        this.label1.TabIndex = 2;
        this.label1.Text = "User Name";
        this.label1.Click += new System.EventHandler(this.label1_Click);
        // 
        // label2
        // 
        this.label2.AutoSize = true;
        this.label2.Location = new System.Drawing.Point(33, 89);
        this.label2.Name = "label2";
        this.label2.Size = new System.Drawing.Size(53, 13);
        this.label2.TabIndex = 3;
        this.label2.Text = "Password";
        // 
        // button1
        // 
        this.button1.Location = new System.Drawing.Point(45, 164);
        this.button1.Name = "button1";
        this.button1.Size = new System.Drawing.Size(75, 24);
        this.button1.TabIndex = 4;
        this.button1.Text = "Log In";
        this.button1.UseVisualStyleBackColor = true;
        this.button1.Click += new System.EventHandler(this.button1_Click);
        // 
        // LogIn
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.ClientSize = new System.Drawing.Size(284, 261);
        this.Controls.Add(this.button1);
        this.Controls.Add(this.label2);
        this.Controls.Add(this.label1);
        this.Controls.Add(this.passTextBox);
        this.Controls.Add(this.userTextBox);
        this.Name = "LogIn";
        this.Text = "Form1";
        this.ResumeLayout(false);
        this.PerformLayout();

    }


}

