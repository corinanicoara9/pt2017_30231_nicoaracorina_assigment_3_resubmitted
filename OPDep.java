package model;

public class OPDep {

	public partial class ViewProd : Form
    {
        int id;
        OperatiiAngajat opang;
        public ViewProd(int id)
        {
            InitializeComponent();
            this.id = id;
        }

        private void ViewProd_Load(object sender, EventArgs e)
        {
            opang = new OperatiiAngajat();
            dataGridView1.AutoGenerateColumns = true;
            dataGridView1.DataSource = opang.GetOrderedProducts(this.id);
           
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            int id;
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
            {
                id = Int16.Parse(row.Cells[0].Value.ToString());

                int cantitate = opang.returnCantitate(id);
               // MessageBox.Show("aaa");
                label2.Text = cantitate.ToString();
            }
        }
    }
}
