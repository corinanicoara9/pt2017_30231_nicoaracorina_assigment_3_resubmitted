package model;

public class Produs {


    public partial class Product : Form
    {
        OperatiiAngajat opAng;
        AddProduct addproduct;
        UpdateProduct updateproduct;
        int id;
        string title;
        string description;
        string color;
        int price;
        int stock;
        public Product()
        {
            InitializeComponent();
        }

        private void Product_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'ordermanagementDataSet1.product' table. You can move, or remove it, as needed.
            this.productTableAdapter.Fill(this.ordermanagementDataSet1.product);

        }

        private void viewButton_Click(object sender, EventArgs e)
        {
            opAng = new OperatiiAngajat();
            dataGridView1.AutoGenerateColumns = true;
            dataGridView1.DataSource = opAng.getProducts();
        }

        private void createButton_Click(object sender, EventArgs e)
        {
            addproduct = new AddProduct();
            addproduct.Show();
        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
            {
                id = Int16.Parse(row.Cells[0].Value.ToString());
                title = row.Cells[1].Value.ToString();
                description = row.Cells[2].Value.ToString();
                color = row.Cells[3].Value.ToString();
                price = Int16.Parse(row.Cells[4].Value.ToString());
                stock = Int16.Parse(row.Cells[5].Value.ToString());


            }
            updateproduct = new UpdateProduct(id,title,description,color,price,stock);
            updateproduct.Show();
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
            {
                int id = Int16.Parse(row.Cells[0].Value.ToString());
                string title = row.Cells[1].Value.ToString();
                string description = row.Cells[2].Value.ToString();
                string color = row.Cells[3].Value.ToString();
              
                int price = Int16.Parse(row.Cells[4].Value.ToString());
                int stock = Int16.Parse(row.Cells[5].Value.ToString());

                opAng = new OperatiiAngajat();
                opAng.deleteProduct(id, title, description, color, price, stock);

              ;
            }
        }
    }
}
