/**
 * 
 */
package model;

/**
 * @author Lenovo
 *
 */
public class Comp {

	/**
	 * @param args
	 */

    public partial class ProductToOrders : Form
    {
        int orderid;
        int stock;
        int cantitate;
        OperatiiAngajat opang;
        public ProductToOrders(int id)
        {
            InitializeComponent();
            this.orderid = id;
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
            {
                int id = Int16.Parse(row.Cells[0].Value.ToString());
                string title = row.Cells[1].Value.ToString();
                string description = row.Cells[2].Value.ToString();
                string color = row.Cells[3].Value.ToString();

                int price = Int16.Parse(row.Cells[4].Value.ToString());
                stock = Int16.Parse(row.Cells[5].Value.ToString());

                // ProductsData p = new ProductsData(id, title, description, color, price, stock);
                cantitate = Int16.Parse(numericUpDown1.Value.ToString());
                opang = new OperatiiAngajat();
                if (opang.existaProdus(id, title, description, color, price, stock, cantitate, orderid))
                    MessageBox.Show("The order exists");
                else
                    opang.addProductToOrder(id, title, description, color, price, stock, cantitate, orderid);



            }
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
                stock = Int16.Parse(row.Cells[5].Value.ToString());
            if (numericUpDown1.Value.Equals(stock)) numericUpDown1.Maximum = stock;
        }

        private void ProductToOrders_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'ordermanagementDataSet1.product' table. You can move, or remove it, as needed.
            this.productTableAdapter.Fill(this.ordermanagementDataSet1.product);

        }
    }
}
