package view;

public class PageClienti {

	public partial class Angajat : Form
    {  LogIn login;
        Order order;
        Product product;
        public Angajat()
        { 
            InitializeComponent();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            order = new Order();
            order.Show();
            
        }

        private void Angajat_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            login = new LogIn();
            this.Close();
            login.Show();
        }

        private void productInformation_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            product = new Product();
            product.Show();
        }
    }
}

