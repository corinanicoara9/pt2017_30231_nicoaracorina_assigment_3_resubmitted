package model;

public class ComandaPDF {

	public partial class Reports : Form
    {
        public Reports()
        {
            InitializeComponent();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Reports_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'ordermanagementDataSet5.report' table. You can move, or remove it, as needed.
            this.reportTableAdapter.Fill(this.ordermanagementDataSet5.report);

        }
    }
}
