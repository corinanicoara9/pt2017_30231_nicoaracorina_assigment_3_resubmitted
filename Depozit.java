package model;

public class Depozit {

	 public partial class Administrator : Form
	    {
	       

	        OperatiiAdministrator opAdmin;
	        AddUser adduser;
	        UpdateUser updateuser;
	        int id;
	        string firstname;
	        string lastname;
	        int type;
	        string username;
	        string password;
	        LogIn li;
	        public Administrator()
	        {
	            InitializeComponent();
	        }

	        private void Administrator_Load(object sender, EventArgs e)
	        {
	            // TODO: This line of code loads data into the 'ordermanagementDataSet4.user' table. You can move, or remove it, as needed.
	            this.userTableAdapter1.Fill(this.ordermanagementDataSet4.user);
	            // TODO: This line of code loads data into the 'ordermanagementDataSet3.user' table. You can move, or remove it, as needed.
	            this.userTableAdapter.Fill(this.ordermanagementDataSet3.user);
	           

	        }

	        private void viewButton_Click(object sender, EventArgs e)
	        {
	            opAdmin = new OperatiiAdministrator();
	            dataGridView1.AutoGenerateColumns = true;
	            dataGridView1.DataSource = opAdmin.getUser();
	        }

	        private void createButton_Click(object sender, EventArgs e)
	        {
	            adduser =new  AddUser();
	            adduser.Show();
	        }

	        private void updateButton_Click(object sender, EventArgs e)
	        {
	            foreach (DataGridViewRow row in dataGridView1.SelectedRows)
	            {
	                id = Int16.Parse(row.Cells[0].Value.ToString());
	                firstname = row.Cells[1].Value.ToString();
	                lastname= row.Cells[2].Value.ToString();
	              
	                type= Int16.Parse(row.Cells[3].Value.ToString());
	                username = row.Cells[4].Value.ToString();
	                password = row.Cells[5].Value.ToString();

	            }
	            updateuser = new UpdateUser(id,firstname,lastname,type,username,password);
	            updateuser.Show();
	        }

	        private void deleteButton_Click(object sender, EventArgs e)
	        { foreach (DataGridViewRow row in dataGridView1.SelectedRows)
	            {
	                id = Int16.Parse(row.Cells[0].Value.ToString());
	                firstname = row.Cells[1].Value.ToString();
	                lastname= row.Cells[2].Value.ToString();
	              
	                type= Int16.Parse(row.Cells[3].Value.ToString());
	                username = row.Cells[4].Value.ToString();
	                password = row.Cells[5].Value.ToString();

	            }
	                opAdmin=new OperatiiAdministrator();
	                opAdmin.deleteUser(id,firstname,lastname,type,username,password);

	              ;
	            }

	        private void button1_Click(object sender, EventArgs e)
	        {
	            Reports r = new Reports();
	            r.Show();
	        }

	        private void logoutButton_Click(object sender, EventArgs e)
	        {
	            li = new LogIn();
	            li.Show();
	            this.Hide();
	        }
	        }

	      
	    }

