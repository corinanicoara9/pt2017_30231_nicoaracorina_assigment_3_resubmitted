package model;

public class Main {


    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.usernameTextBox = new System.Windows.Forms.TextBox();
        this.lastnameTextBox = new System.Windows.Forms.TextBox();
        this.passwordTextBox = new System.Windows.Forms.TextBox();
        this.typeTextBox = new System.Windows.Forms.TextBox();
        this.firstnameTextBox = new System.Windows.Forms.TextBox();
        this.label1 = new System.Windows.Forms.Label();
        this.label2 = new System.Windows.Forms.Label();
        this.label3 = new System.Windows.Forms.Label();
        this.label4 = new System.Windows.Forms.Label();
        this.label5 = new System.Windows.Forms.Label();
        this.okButton = new System.Windows.Forms.Button();
        this.SuspendLayout();
        // 
        // usernameTextBox
        // 
        this.usernameTextBox.Location = new System.Drawing.Point(86, 156);
        this.usernameTextBox.Name = "usernameTextBox";
        this.usernameTextBox.Size = new System.Drawing.Size(100, 20);
        this.usernameTextBox.TabIndex = 0;
        this.usernameTextBox.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
        // 
        // lastnameTextBox
        // 
        this.lastnameTextBox.Location = new System.Drawing.Point(86, 66);
        this.lastnameTextBox.Name = "lastnameTextBox";
        this.lastnameTextBox.Size = new System.Drawing.Size(100, 20);
        this.lastnameTextBox.TabIndex = 1;
        // 
        // passwordTextBox
        // 
        this.passwordTextBox.Location = new System.Drawing.Point(86, 205);
        this.passwordTextBox.Name = "passwordTextBox";
        this.passwordTextBox.Size = new System.Drawing.Size(100, 20);
        this.passwordTextBox.TabIndex = 2;
        // 
        // typeTextBox
        // 
        this.typeTextBox.Location = new System.Drawing.Point(86, 113);
        this.typeTextBox.Name = "typeTextBox";
        this.typeTextBox.Size = new System.Drawing.Size(100, 20);
        this.typeTextBox.TabIndex = 3;
        // 
        // firstnameTextBox
        // 
        this.firstnameTextBox.Location = new System.Drawing.Point(86, 23);
        this.firstnameTextBox.Name = "firstnameTextBox";
        this.firstnameTextBox.Size = new System.Drawing.Size(100, 20);
        this.firstnameTextBox.TabIndex = 4;
        // 
        // label1
        // 
        this.label1.AutoSize = true;
        this.label1.Location = new System.Drawing.Point(26, 23);
        this.label1.Name = "label1";
        this.label1.Size = new System.Drawing.Size(57, 13);
        this.label1.TabIndex = 5;
        this.label1.Text = "First Name";
        // 
        // label2
        // 
        this.label2.AutoSize = true;
        this.label2.Location = new System.Drawing.Point(26, 66);
        this.label2.Name = "label2";
        this.label2.Size = new System.Drawing.Size(56, 13);
        this.label2.TabIndex = 6;
        this.label2.Text = "Last name";
        // 
        // label3
        // 
        this.label3.AutoSize = true;
        this.label3.Location = new System.Drawing.Point(26, 113);
        this.label3.Name = "label3";
        this.label3.Size = new System.Drawing.Size(31, 13);
        this.label3.TabIndex = 7;
        this.label3.Text = "Type";
        // 
        // label4
        // 
        this.label4.AutoSize = true;
        this.label4.Location = new System.Drawing.Point(26, 205);
        this.label4.Name = "label4";
        this.label4.Size = new System.Drawing.Size(53, 13);
        this.label4.TabIndex = 8;
        this.label4.Text = "Password";
        // 
        // label5
        // 
        this.label5.AutoSize = true;
        this.label5.Location = new System.Drawing.Point(26, 159);
        this.label5.Name = "label5";
        this.label5.Size = new System.Drawing.Size(55, 13);
        this.label5.TabIndex = 9;
        this.label5.Text = "Username";
        // 
        // okButton
        // 
        this.okButton.Location = new System.Drawing.Point(86, 252);
        this.okButton.Name = "okButton";
        this.okButton.Size = new System.Drawing.Size(75, 23);
        this.okButton.TabIndex = 10;
        this.okButton.Text = "Ok";
        this.okButton.UseVisualStyleBackColor = true;
        this.okButton.Click += new System.EventHandler(this.okButton_Click);
        // 
        // UpdateUser
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.ClientSize = new System.Drawing.Size(376, 296);
        this.Controls.Add(this.okButton);
        this.Controls.Add(this.label5);
        this.Controls.Add(this.label4);
        this.Controls.Add(this.label3);
        this.Controls.Add(this.label2);
        this.Controls.Add(this.label1);
        this.Controls.Add(this.firstnameTextBox);
        this.Controls.Add(this.typeTextBox);
        this.Controls.Add(this.passwordTextBox);
        this.Controls.Add(this.lastnameTextBox);
        this.Controls.Add(this.usernameTextBox);
        this.Name = "UpdateUser";
        this.Text = "UpdateUser";
        this.ResumeLayout(false);
        this.PerformLayout();

    }

  
}